#!/bin/bash
# rsync script

#Validate if root
#if [ "$(id -u)" != "0" ]; then
#   echo -e "\e[00;31mThis script must be run as root\e[00m" 1>&2
#   exit 1
#fi

VAGRANT=$1
CHECKOUT=_projects
PROJECTNAME=$2
LOCALPROJECTNAME=$3
REPONAME=$4



#|| -z $DBNAME || -z $DBUSER || -z $DBPASS

if [[ -z $VAGRANT || -z $PROJECTNAME || -z $LOCALPROJECTNAME || -z $REPONAME ]];

  then
    echo -e "\e[00;31mPlease Provide path to VAGRANT folder (example: /var/www/vagrant/) as 1st variable\e[00m"
    echo -e "\e[00;31mPlease Provide Site Name inside CheckOut Folder (example: domain.spotlight-verlag.com) as 2nd Variable\e[00m"
    echo -e "\e[00;31mPlease Provide local project name. (example: domain.local) as 3rd Variable\e[00m"
    echo -e "\e[00;31mPlease Provide Repository Name as 4th Variable\e[00m"
    printf "\n"
    echo -e "\e[00;33mEXAMPLE: ./install_virtualhsot_mainweb.sh /vagrant/ [PROJECTNAME] [LOCALPROJECTNAME] [REPONAME]\e[00m"
    echo -e "\e[00;34mEXAMPLE: ./install_virtualhsot_mainweb.sh /vagrant/ projectname.spotlight-velag.de projectname platform-backend.spotlight-verlag.de.git \e[00m"
    exit
  else
   echo ' '
fi

echo -e "\e[1m\e[32mThis script will do the followings.\e[0m"
echo -e "\e[33m 01. Clone the repository to your local."
echo -e "\e[33m 02. Update the hosts file.\e[00m"

install_virtualhost() {
    #Variables
    APACHEFOLDER='/etc/apache2'

    Updates outside the Vagrant - STARTS
    echo '**************************************************************'
    echo "**** Updates INSIDE the Vagrant - STARTS ****"
    echo '**************************************************************'

    #01. Clone git to Project Directory
    echo ' '
    echo "01. Clonning the repository."

    if [ ! -d "${VAGRANT}_projects/${PROJECTNAME}" ]; then
        if ! (git clone git@bitbucket.org:spotlightdev/$REPONAME ${VAGRANT}_projects/${PROJECTNAME}) then
            echo -e "\e[00;31mClone is Failed .. No file is updated. EXITING\e[00m"
            exit 1
            # Put Failure actions here...
        else
            cd ${VAGRANT}_projects/${PROJECTNAME}
            git fetch --all
            git checkout master
            echo "Clone is Success"
            # Put Success actions here...
        fi
    else
        echo -e "\e[00;31m ${PROJECTNAME} Directorty is aleady there.\e[00m"
    fi

    #02. Creating entry in /etc/hosts
    echo ' '
    echo "02. Creating entry in /etc/hosts"
    
    HOSTS=`grep -o $LOCALPROJECTNAME /etc/hosts | head -n1`
    if [ "$HOSTS" == "$LOCALPROJECTNAME" ]; then
        echo -e "\e[00;31mHosts file already contains entry for $LOCALPROJECTNAME skipping entry.\e[00m"
    else

        if (sudo sh -c "echo 192.168.33.10 $LOCALPROJECTNAME >> /etc/hosts") then
            echo -e "\e[00;32m Entry $LOCALPROJECTNAME added to hosts file \e[00m"
        else
            echo -e "\e[00;31m Entry $LOCALPROJECTNAME FAILED to hosts file \e[00m"
        fi
    fi

}

read -p "This script needs to be run in you local. Are you sure? (y/n)" -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
    then
        install_virtualhost
    else
       echo -e "\e[00;31m Run this script inside your local mahcine. \e[00m"
    exit
fi


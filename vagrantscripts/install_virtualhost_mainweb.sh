#!/bin/bash
# rsync script

#Validate if root
if [ "$(id -u)" != "0" ]; then
   echo -e "\e[00;31mThis script must be run as root\e[00m" 1>&2
   exit 1
fi

VAGRANT=$1
CHECKOUT=_projects
PROJECTNAME=$2
LOCALPROJECTNAME=$3



#|| -z $DBNAME || -z $DBUSER || -z $DBPASS

if [[ -z $VAGRANT || -z $PROJECTNAME || -z $LOCALPROJECTNAME ]];

  then
    echo -e "\e[00;31mPlease Provide path to VAGRANT folder (example: /var/www/vagrant/) as 1st variable\e[00m"
    echo -e "\e[00;31mPlease Provide Site Name inside CheckOut Folder (example: domain.spotlight-verlag.com) as 2nd Variable\e[00m"
    echo -e "\e[00;31mPlease Provide local project name. (example: domain.local) as 3rd Variable\e[00m"
    printf "\n"
    echo -e "\e[00;33mEXAMPLE: ./install_virtualhsot_mainweb.sh /vagrant/ [PROJECTNAME] [LOCALPROJECTNAME]\e[00m"
    echo -e "\e[00;34mEXAMPLE: ./install_virtualhsot_mainweb.sh /vagrant/ projectname.spotlight-velag.de projectname\e[00m"
    exit
  else
   echo ' '
fi

echo -e "\e[1m\e[32mThis script will do the followings.\e[0m"
echo -e "\e[33m 01. Add configurations to sites-available"
echo -e "\e[33m 02. Restart the apache\e[00m"

install_virtualhost() {
    #Variables
    APACHEFOLDER='/etc/apache2'

    Updates outside the Vagrant - STARTS
    echo '**************************************************************'
    echo "**** Updates INSIDE the Vagrant - STARTS ****"
    echo '**************************************************************'

    #01. Updating $VAGRANT.provisioning/vhosts_mainweb
    echo ' '
    echo "01. Updating ${APACHEFOLDER}/sites-available/vhosts_mainweb.conf"
    
    HOSTS=`grep -o $PROJECTNAME ${APACHEFOLDER}/sites-available/vhosts_mainweb.conf | head -n1`
    if [ "$HOSTS" == "$PROJECTNAME" ]; then
        echo -e "\e[00;31m vhosts_mainweb.conf file already contains entry for $PROJECTNAME skipping entry.\e[00m"
    else
        echo "Crating new entry for $PROJECTNAME in vhosts_mainweb.conf"
        echo "
<VirtualHost *:443>
        ServerAdmin webmaster@localhost
        ServerName $LOCALPROJECTNAME
        SSLEngine on
        SSLCertificateFile /vagrant/.provisioning/ssl/$LOCALPROJECTNAME.crt
        SSLCertificateKeyFile /vagrant/.provisioning/ssl/$LOCALPROJECTNAME.key
        DocumentRoot /vagrant/_projects/$PROJECTNAME/web
        LogLevel debug
        ErrorLog /var/log/apache2/error.log
        CustomLog /var/log/apache2/access.log combined
        <Directory /vagrant/_projects/$PROJECTNAME/web>
            Options Indexes FollowSymlinks
            AllowOverride All
            Require all granted
        </Directory>
</VirtualHost>" >> ${APACHEFOLDER}/sites-available/vhosts_mainweb.conf

    fi
    
    echo "Restarting apache"
    sudo service apache2 restart
}

read -p "This script needs to be run in mainweb. Are you sure? (y/n)" -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
    then
        install_virtualhost
    else
       echo -e "\e[00;31m Run this script inside mainweb. \e[00m"
    exit
fi


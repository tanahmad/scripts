#!/bin/bash
# rsync script

#Validate if root
#if [ "$(id -u)" != "0" ]; then
#   echo "This script must be run as root" 1>&2
#   exit 1
#fi

VAGRANT=$1
CHECKOUT=_projects
PROJECTNAME=$2
LOCALPROJECTNAME=$3
REPONAME=$4
DUMPFILE=$5
DUMPDB=$6
REMOTEID=$7


#|| -z $DBNAME || -z $DBUSER || -z $DBPASS

if [[ -z $VAGRANT || -z $PROJECTNAME || -z $LOCALPROJECTNAME || -z $REPONAME || -z $DUMPFILE || -z $DUMPDB || -z $REMOTEID ]];

  then
    echo -e "\e[00;31mPlease Provide path to VAGRANT folder (example: /var/www/vagrant/) as 1st variable\e[00m"
    #echo -e "\e[00;31mPlease Provide a folder to check out the project (example: _projects) as 1st variable\e[00m"
    echo -e "\e[00;31mPlease Provide Site Name inside CheckOut Folder (example: domain.spotlight-verlag.com) as 2nd Variable\e[00m"
    echo -e "\e[00;31mPlease Provide local project name. (example: domain.local) as 3rd Variable\e[00m"
    echo -e "\e[00;31mPlease Provide Repository Name as 4th Variable\e[00m"
    echo -e "\e[00;31mPlease Provide DUMPFILE Name as 5th Variable\e[00m"
    echo -e "\e[00;31mPlease Provide DUMPDB Name as 6th Variable\e[00m"
    echo -e "\e[00;31mPlease Provide REMOTEID as 7th Variable\e[00m"
    printf "\n"
    echo -e "\e[00;31mEXAMPLE: ./install_virtualhsot.sh [VAGRANT] [PROJECTNAME] [LOCALPROJECTNAME] [REPONAME] [DUMPFILE] [DUMPDB] [REMOTEID]\e[00m"
    echo -e "\e[00;34mEXAMPLE: ./install_virtualhsot.sh /var/www/vagrant/ projectname.spotlight-velag.de projectname platform-backend.spotlight-verlag.de.git projectname.spotlight-verlag.de.sql.gz projectname_services projectname \e[00m"
    exit
  else
   echo ' '
fi

echo -e "\e[1m\e[32mThis script will do the followings.\e[0m"
echo -e "\e[33m 01. Clone the repository to your local."
echo -e "\e[33m 02. Adding git clone scrip to .provisioning/gitsetup_mainweb.sh"
echo -e "\e[33m 03. Updating $VAGRANT.provisioning/vhosts_mainweb"
echo -e "\e[33m 04. Creating SSL for the project name"
echo -e "\e[33m 05. Updating the ${VAGRANT}.provisioning/dbsetup.sh for the database information"
echo -e "\e[33m 06. Creating entry in /etc/hosts \e[00m"



install_virtualhost() {
    #Variables
    APACHEFOLDER='/etc/apache2'

     Updates outside the Vagrant - STARTS
    echo '**************************************************************'
    echo "**** Updates outside the Vagrant (In your local) - STARTS ****"
    echo '**************************************************************'


    #Clone git to Project Directory
    echo ' '
    echo "01. Clonning the repository."

    if [ ! -d "${VAGRANT}_projects/${PROJECTNAME}" ]; then
        if ! (git clone git@bitbucket.org:spotlightdev/$REPONAME ${VAGRANT}_projects/${PROJECTNAME}) then
            echo -e "\e[00;31mClone is Failed .. No file is updated. EXITING\e[00m"
            exit 1
            # Put Failure actions here...
        else
            cd ${VAGRANT}_projects/${PROJECTNAME}
            git fetch --all
            git checkout master
            echo "Clone is Success"
            # Put Success actions here...
        fi
    else
        echo -e "\e[00;31m ${PROJECTNAME} Directorty is aleady there.\e[00m"
    fi


    # 02. Adding git clone scrip to .provisioning/gitsetup_mainweb.sh
    echo ' '
    echo "Adding git clone scrip to ${VAGRANT}.provisioning/gitsetup_mainweb.sh"
    

    HOSTS=`grep -o $PROJECTNAME ${VAGRANT}.provisioning/gitsetup_mainweb.sh | head -n1`
    if [ "$HOSTS" == "$PROJECTNAME" ]; then
        echo -e "\e[00;31m gitsetup_mainweb.sh file already contains entry for $PROJECTNAME skipping entry.\e[00m"
    else
        echo "Crating new entry for $REPONAME in gitsetup_mainweb.sh"
        echo "
# $PROJECTNAME
if [ ! -d /vagrant/_projects/$PROJECTNAME ]; then
    git clone git@bitbucket.org:spotlightdev/$REPONAME /vagrant/_projects/$PROJECTNAME
    cd /vagrant/_projects/$PROJECTNAME
    git fetch --all
    git checkout master
if" >> ${VAGRANT}.provisioning/gitsetup_mainweb.sh

    fi



    # 03. Updating $VAGRANT.provisioning/vhosts_mainweb
    echo ' '
    echo "03. Updating $VAGRANT.provisioning/vhosts_mainweb"
    
    HOSTS=`grep -o $PROJECTNAME ${VAGRANT}.provisioning/vhosts_mainweb | head -n1`
    if [ "$HOSTS" == "$PROJECTNAME" ]; then
        echo -e "\e[00;31m vhosts_mainweb file already contains entry for $PROJECTNAME skipping entry.\e[00m"
    else
        echo "Crating new entry for $PROJECTNAME in vhosts_mainweb"
        echo "
<VirtualHost *:443>
        ServerAdmin webmaster@localhost
        ServerName $LOCALPROJECTNAME
        SSLEngine on
        SSLCertificateFile /vagrant/.provisioning/ssl/$LOCALPROJECTNAME.crt
        SSLCertificateKeyFile /vagrant/.provisioning/ssl/$LOCALPROJECTNAME.key
        DocumentRoot /vagrant/_projects/$PROJECTNAME/web
        LogLevel debug
        ErrorLog /var/log/apache2/error.log
        CustomLog /var/log/apache2/access.log combined
        <Directory /vagrant/_projects/$PROJECTNAME/web>
            Options Indexes FollowSymlinks
            AllowOverride All
            Require all granted
        </Directory>
</VirtualHost>" >> ${VAGRANT}.provisioning/vhosts_mainweb

    fi

    # 04. Creating SSL for the project name
    echo ' '
    echo "04. Creating SSL for the project name"
    cd $VAGRANT.provisioning/ssl
    lines=$(find . -name $LOCALPROJECTNAME'*' | wc -l)
    if [ $lines -eq 0 ]; then
        echo "$LOCALPROJECTNAME.key File NOT found, now creating."
        echo -e "\e[00;31m $LOCALPROJECTNAME.key File NOT found, now creating.\e[00m"
        ${VAGRANT}.provisioning/install_ssl_crt.sh $LOCALPROJECTNAME
    else
        echo -e "\e[00;32m $LOCALPROJECTNAME.key File found. Skipping creating ssl.\e[00m"
        
    fi


    #05. Updating the ${VAGRANT}.provisioning/dbsetup.sh for the database information
    echo ' '
    echo "05. Updating the ${VAGRANT}.provisioning/dbsetup.sh for the database information"
    
    HOSTS=`grep -o ${DUMPFILE} ${VAGRANT}.provisioning/dbsetup.sh | head -n1`
    if [ "$HOSTS" == "${DUMPFILE}" ]; then
        echo -e "\e[00;31m ${VAGRANT}.provisioning/dbsetup.sh file already contains entry for ${DUMPFILE} skipping entry.\e[00m"
    else
        echo "# $PROJECTNAME
DUMPFILE=${DUMPFILE}
DUMPDB=${DUMPDB}
REMOTEID=${REMOTEID}
grabit" >> ${VAGRANT}.provisioning/dbsetup.sh
    
    fi

    #06. Creating entry in /etc/hosts
    echo ' '
    echo "06. Creating entry in /etc/hosts"
    
    HOSTS=`grep -o $LOCALPROJECTNAME /etc/hosts | head -n1`
    if [ "$HOSTS" == "$LOCALPROJECTNAME" ]; then
        echo -e "\e[00;31mHosts file already contains entry for $LOCALPROJECTNAME skipping entry.\e[00m"
    else

        if (sudo sh -c "echo 192.168.33.10 $LOCALPROJECTNAME >> /etc/hosts") then
            echo -e "\e[00;32m Entry $LOCALPROJECTNAME added to hosts file \e[00m"
        else
            echo -e "\e[00;31m Entry $LOCALPROJECTNAME FAILED to hosts file \e[00m"
        fi
    fi

}

read -p "This script is for lcoal machine. Are you sure? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
    then
        install_virtualhost
    else
       echo -e "\e[00;31m Run this script on your local machine. \e[00m"
    exit
fi


#!/bin/bash

#Required
domain=$1
commonname=$domain

#Change to your company details
country=DE
state=Bayern
locality=Munich
organization=SpotlightVerlag
organizationalunit=IT
email=t.ahmad@spotlight-verlag.de

#Optional
password=dummypassword

if [ -z "$domain" ]
then
    echo "DomainName not present."
    echo "Useage $0 [common name]"

    exit 99
fi

echo "Generating key request for $domain"

#Generate a key
openssl genrsa -des3 -passout pass:$password -out $domain.key 2048 -noout

#Remove passphrase from the key. Comment the line out to keep the passphrase
echo "Removing passphrase from key"
openssl rsa -in $domain.key -passin pass:$password -out $domain.key


#Create the Certificate
echo "Creating CRT"

#openssl req -x509 -newkey rsa:2048 -days 365 -keyout /etc/ssl/private/apache-selfsigned.key -out /etc/ssl/certs/apache-selfsigned.crt -nodes
#openssl req -x509 -newkey rsa:4096 -days 3650 -keyout key.pem -out cert.pem -subj "/C=$country/ST=$state/L=$locality/O=$organization/OU=$organizationalunit/CN=$commonname/emailAddress=$email"
openssl req -x509 -newkey rsa:4096 -days 3650 -keyout $domain.key -out $domain.crt -subj "/C=$country/ST=$state/L=$locality/O=$organization/OU=$organizationalunit/CN=$commonname/emailAddress=$email"

#openssl req -new -key $domain.key -out $domain.csr -passin pass:$password \
#    -subj "/C=$country/ST=$state/L=$locality/O=$organization/OU=$organizationalunit/CN=$commonname/emailAddress=$email"
echo "--------------------------------"
echo "-----$domain.crt is created-----"
echo "--------------------------------"
echo


echo
echo "--------------------------------"
echo "-----$domain.key is created-----"
echo "--------------------------------"
echo


#echo "---------------------------"
#echo "-----Below is your CSR-----"
#echo "---------------------------"
#echo
#cat $domain.csr
# 
#echo
#echo "---------------------------"
#echo "-----Below is your Key-----"
#echo "---------------------------"
#echo
#cat $domain.key
